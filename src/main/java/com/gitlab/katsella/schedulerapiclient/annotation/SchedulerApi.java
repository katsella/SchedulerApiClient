package com.gitlab.katsella.schedulerapiclient.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SchedulerApi {
    String name() default "";
}
