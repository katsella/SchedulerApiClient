package com.gitlab.katsella.schedulerapiclient.config;

import com.gitlab.katsella.schedulerapiclient.controller.SchedulerApiController;
import com.gitlab.katsella.schedulerapiclient.processor.AnnotationFinder;
import com.gitlab.katsella.schedulerapiclient.processor.Request.RequestProcessor;
import com.gitlab.katsella.schedulerapiclient.processor.Request.impl.RestTemplateRequestProcessor;
import com.gitlab.katsella.schedulerapiclient.processor.SchedulerApiJobDoneProcessor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component("SchedulerApiBeanLoader")
public class SchedulerApiBeanLoader {

    private final SchedulerApiController schedulerApiController;

    private final SchedulerApiJobDoneProcessor schedulerApiJobDoneProcessor;

    private final AnnotationFinder annotationFinder;

    private final SchedulerApiConfig schedulerApiConfig;

    private final RequestProcessor requestProcessor;

    public SchedulerApiBeanLoader(SchedulerApiController schedulerApiController, AnnotationFinder annotationFinder, SchedulerApiConfig schedulerApiConfig, @Nullable RequestProcessor requestProcessor) {
        if (Objects.isNull(requestProcessor))
            this.requestProcessor = new RestTemplateRequestProcessor();
        else
            this.requestProcessor = requestProcessor;

        this.schedulerApiConfig = schedulerApiConfig;
        this.schedulerApiController = schedulerApiController;
        this.annotationFinder = annotationFinder;
        this.schedulerApiJobDoneProcessor = new SchedulerApiJobDoneProcessor(schedulerApiConfig, this.requestProcessor);
    }

    public SchedulerApiController getSchedulerApiController() {
        return schedulerApiController;
    }

    public SchedulerApiJobDoneProcessor getSchedulerApiJobDoneProcessor() {
        return schedulerApiJobDoneProcessor;
    }

    public AnnotationFinder getAnnotationFinder() {
        return annotationFinder;
    }

    public SchedulerApiConfig getSchedulerApiConfig() {
        return schedulerApiConfig;
    }

    public RequestProcessor getRequestProcessor() {
        return requestProcessor;
    }
}
