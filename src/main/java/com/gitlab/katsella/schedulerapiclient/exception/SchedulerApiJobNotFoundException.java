package com.gitlab.katsella.schedulerapiclient.exception;

public class SchedulerApiJobNotFoundException extends SchedulerApiException {
    public SchedulerApiJobNotFoundException(String msg) {
        super(msg);
    }
}
