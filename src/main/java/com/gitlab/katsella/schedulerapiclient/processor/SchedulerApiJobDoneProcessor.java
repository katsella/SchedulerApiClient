package com.gitlab.katsella.schedulerapiclient.processor;

import com.gitlab.katsella.schedulerapiclient.config.SchedulerApiConfig;
import com.gitlab.katsella.schedulerapiclient.model.SchedulerApiBody;
import com.gitlab.katsella.schedulerapiclient.processor.Request.RequestProcessor;
import com.gitlab.katsella.schedulerapiclient.processor.Request.model.RequestModel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SchedulerApiJobDoneProcessor implements ISchedulerApiJobDone {

    private static final Logger LOGGER = Logger.getLogger(SchedulerApiJobDoneProcessor.class.getName());

    private final int THREAD_COUNT = 50;
    private final int THREAD_SLEEP_ON_ERROR_MILLISECONDS = 2000;
    private final int THREAD_TRY_COUNT = 200;

    private RequestProcessor requestProcessor;

    private SchedulerApiConfig schedulerApiConfig;

    private ExecutorService taskExecutor;

    public SchedulerApiJobDoneProcessor(SchedulerApiConfig schedulerApiConfig, RequestProcessor requestProcessor) {
        this.schedulerApiConfig = schedulerApiConfig;
        this.requestProcessor = requestProcessor;
        taskExecutor = Executors.newFixedThreadPool(THREAD_COUNT);
    }

    public void jobDone(SchedulerApiBody body) {
        taskExecutor.execute(() -> {
            reTryThreaded(body);
        });
    }

    public void reTryThreaded(SchedulerApiBody body) {
        int errorCount = 0;
        while (errorCount < THREAD_TRY_COUNT) {
            try {
                RequestModel<SchedulerApiBody> requestModel = new RequestModel();
                requestModel.setBody(body);
                requestModel.setUrl(schedulerApiConfig.getJobDoneUrl());
                requestProcessor.post(requestModel, String.class);
                break;
            } catch (Exception e) {
                if (errorCount++ % 30 == 0)
                    LOGGER.log(Level.SEVERE, "SchedulerApiJobDone::error errorMessage: " + e.getMessage(), e);
                try {
                    Thread.sleep(THREAD_SLEEP_ON_ERROR_MILLISECONDS);
                } catch (InterruptedException ex) {
                }
            }
        }

    }
}
