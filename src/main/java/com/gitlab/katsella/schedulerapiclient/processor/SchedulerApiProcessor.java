package com.gitlab.katsella.schedulerapiclient.processor;

import com.gitlab.katsella.schedulerapiclient.annotation.EnableSchedulerApi;
import com.gitlab.katsella.schedulerapiclient.annotation.SchedulerApi;
import com.gitlab.katsella.schedulerapiclient.exception.SchedulerApiException;
import com.gitlab.katsella.schedulerapiclient.exception.SchedulerApiJobNotFoundException;
import com.gitlab.katsella.schedulerapiclient.model.ObjectMethod;
import com.gitlab.katsella.schedulerapiclient.model.SchedulerApiBody;
import com.gitlab.katsella.schedulerapiclient.util.StringUtils;
import org.springframework.util.CollectionUtils;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SchedulerApiProcessor {

    private static final Logger LOGGER = Logger.getLogger(SchedulerApiProcessor.class.getName());

    private Map<String, ObjectMethod> schedulerJobs;

    private String schedulerApiEndpoint = "/api/v1/scheduler";

    private Class<? extends Annotation> schedulerApiClass = SchedulerApi.class;

    private boolean schedulerApiEnable = false;

    private AnnotationFinder annotationFinder;

    public SchedulerApiProcessor(AnnotationFinder annotationFinder) {
        this.annotationFinder = annotationFinder;
        EnableSchedulerApi enableSchedulerApi = getSchedulerApiEnable();
        if (Objects.isNull(enableSchedulerApi)) {
            LOGGER.warning("SchedulerApi not enabled. You can enable it with @SchedulerApiEnable");
            return;
        }
        schedulerApiEnable = true;

        schedulerApiEndpoint = enableSchedulerApi.endpointPrefix();

        List<ObjectMethod> objectMethods = annotationFinder.findAnnotatedMethod(schedulerApiClass);

        schedulerJobs = objectMethods.stream().collect(Collectors.toMap(this::mapMethodKey, (e) -> e));

        printSchedulerServices(schedulerJobs);
    }

    private String mapMethodKey(ObjectMethod objectMethod) {
        SchedulerApi schedulerApi = (SchedulerApi) objectMethod.getMethod().getAnnotation(schedulerApiClass);
        if (!StringUtils.isBlank(schedulerApi.name()))
            return schedulerApi.name();
        return getDefaultName(objectMethod);
    }

    private String getDefaultName(ObjectMethod objectMethod) {
        String className = objectMethod.getClazz().getName();
        String[] splits = className.split("\\.");
        className = splits[splits.length - 1];
        String methodName = objectMethod.getMethod().getName();
        return className + "." + methodName;
    }

    private void printSchedulerServices(Map<String, ObjectMethod> schedulerJobs) {
        String text = "SchedulerApi initialized successfully endpoint: " + schedulerApiEndpoint + "\nSchedulerApi Jobs {\n";
        for (String name : schedulerJobs.keySet()) {
            text += name + ",\n";
        }
        text += "}";
        LOGGER.info(text);
    }

    private EnableSchedulerApi getSchedulerApiEnable() {
        List<Class<?>> schedulerApiEnableClassList = annotationFinder.getAnnotationClass(EnableSchedulerApi.class);

        if (CollectionUtils.isEmpty(schedulerApiEnableClassList))
            return null;

        if (schedulerApiEnableClassList.size() > 1)
            throw new SchedulerApiException("More than one class annotated with SchedulerApiEnable");

        EnableSchedulerApi[] enableSchedulerApiArray = schedulerApiEnableClassList.get(0).getAnnotationsByType(EnableSchedulerApi.class);

        if (enableSchedulerApiArray.length == 1)
            return enableSchedulerApiArray[0];
        else if (enableSchedulerApiArray.length > 1)
            throw new SchedulerApiException("More than one SchedulerApiEnable annotated on same class");

        return null;
    }

    public String getSchedulerApiEndpoint() {
        return schedulerApiEndpoint;
    }

    public boolean isSchedulerApiEnable() {
        return schedulerApiEnable;
    }

    public void triggerSchedulerJob(SchedulerApiBody body) throws IllegalAccessException {
        ObjectMethod objectMethod = schedulerJobs.get(body.getName());
        if (Objects.isNull(objectMethod))
            throw new SchedulerApiJobNotFoundException("scheduler job not found. name: " + body.getName());
        objectMethod.invoke();
    }
}
