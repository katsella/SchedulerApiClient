package com.gitlab.katsella.schedulerapiclient.processor;

import com.gitlab.katsella.schedulerapiclient.config.SchedulerApiBeanLoader;
import com.gitlab.katsella.schedulerapiclient.service.SchedulerApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;


@Component
@DependsOn("SchedulerApiBeanLoader")
public class SpringEventListener {

    private SchedulerApiProcessor schedulerApiProcessor;

    private SchedulerApiService schedulerApiService;

    private final SchedulerApiBeanLoader schedulerApiBeanLoader;

    public SpringEventListener(@Autowired SchedulerApiBeanLoader schedulerApiBeanLoader) {
        this.schedulerApiBeanLoader = schedulerApiBeanLoader;
        init();
    }

//    @PostConstruct
    public void init() {
        this.schedulerApiProcessor = new SchedulerApiProcessor(schedulerApiBeanLoader.getAnnotationFinder());
        this.schedulerApiService = new SchedulerApiService(schedulerApiProcessor, schedulerApiBeanLoader.getSchedulerApiJobDoneProcessor());

        if (schedulerApiProcessor.isSchedulerApiEnable())
            schedulerApiBeanLoader.getSchedulerApiController().registerEndpoint(schedulerApiProcessor.getSchedulerApiEndpoint(), schedulerApiService);
    }
}
