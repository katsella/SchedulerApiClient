package com.gitlab.katsella.schedulerapiclient.service;

import com.gitlab.katsella.schedulerapiclient.model.SchedulerApiBody;
import com.gitlab.katsella.schedulerapiclient.processor.ISchedulerApiJobDone;
import com.gitlab.katsella.schedulerapiclient.processor.SchedulerApiProcessor;

import java.lang.reflect.InvocationTargetException;

public class SchedulerApiService {

    private SchedulerApiProcessor schedulerApiProcessor;

    private ISchedulerApiJobDone schedulerApiJobDoneProcessor;

    public SchedulerApiService(SchedulerApiProcessor processor, ISchedulerApiJobDone schedulerApiJobDoneProcessor) {
        this.schedulerApiProcessor = processor;
        this.schedulerApiJobDoneProcessor = schedulerApiJobDoneProcessor;
    }

    public void triggerSchedulerJob(SchedulerApiBody body) throws InvocationTargetException, IllegalAccessException {
        try {
            schedulerApiProcessor.triggerSchedulerJob(body);
        } finally {
            schedulerApiJobDoneProcessor.jobDone(body);
        }
    }
}
